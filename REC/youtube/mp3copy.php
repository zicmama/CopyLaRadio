<!doctype html>
<html>
  <head>
    <title>YouTube Search & Copy</title>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" >
<link href="./bootstrap.css" rel="stylesheet">
<style type="text/css">
body{margin-top: 50px; margin-left: 50px}
</style>
  </head>
  <body>
    <form action="./search.php"  method="GET">
  <div>
    Recherche: <input type="search" id="q" name="q" placeholder="Faites votre recherche">
  </div>
  <div>
    Resultats: <input type="number" id="maxResults" name="maxResults" min="1" max="50" step="1" value="5">
  </div>
  <input type="submit" value="Lancer">
</form>
<?php
include("functions.php");

$id = escapeshellcmd($_REQUEST['id']);
// http://stackoverflow.com/questions/17115867/php-replace-all-non-alphanumeric-chars-for-all-languages-supported
// Must remove accent before calling youtube-dl that strip it.
$artist = removeaccents(ucwords( preg_replace('~\P{Xan}/&()++~u', ' ', $_REQUEST['artist']) ));
$title = removeaccents(ucwords( preg_replace('~\P{Xan}/&()++~u', ' ', $_REQUEST['title']) ));
$type = escapeshellcmd($_REQUEST['type']);

shell_exec("nohup ".$_SERVER['DOCUMENT_ROOT']."/REC/youtube/ytcopy.sh \"$id\" \"$artist\" \"$title\" \"$type\" >> \"/tmp/ALOG-copylaradio.log\" 2>&1 &");

echo "<iframe id='ytplayer' type='text/html' width='320' height='240' src='https://www.youtube.com/embed/".$id."' frameborder='0' allowfullscreen=''></iframe>
      <br><h2>La copie MP3 de <b>".$artist." : ".$title."</b> est en cours...</h2>
      <br>Dans une minute retrouvez votre fichier dans votre <a href='/rompr' target='jukebox'>Jukebox</a>;)";
?>