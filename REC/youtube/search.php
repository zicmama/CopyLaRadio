<?php
include("functions.php");

if (!isset($_GET['maxResults'])) $_GET['maxResults']=3;

if ($_GET['q'] && $_GET['maxResults']) {
  // Call set_include_path() as needed to point to your client library.
  require_once ('google-api-php-client/src/Google_Client.php');
  require_once ('google-api-php-client/src/contrib/Google_YouTubeService.php');

  /* Set $DEVELOPER_KEY to the "API key" value from the "Access" tab of the
  Google APIs Console <http://code.google.com/apis/console#access>
  Please ensure that you have enabled the YouTube Data API for your project. */
  //$DEVELOPER_KEY = 'AIzaSyDOkg-u9jnhP-WnzX5WPJyV1sc5QQrtuyc';
  //$DEVELOPER_KEY = '926204256831-9488tfks7skemb84tgse8ebj1bm0l3nv.apps.googleusercontent.com';
  $DEVELOPER_KEY = 'AIzaSyDoaXnfezu--CWvzHsuqfP8icXep1NB19c';
  // SECRET 9L4LvUQ1lsteG20MXDER2b-D 
  
  $client = new Google_Client();
  $client->setDeveloperKey($DEVELOPER_KEY);

  $youtube = new Google_YoutubeService($client);

  try {
    $searchResponse = $youtube->search->listSearch('id,snippet', array(
      'q' => urldecode($_GET['q']),
      'maxResults' => $_GET['maxResults'],
    ));

    $videos = '';
    $channels = '';
    
    foreach ($searchResponse['items'] as $searchResult) {
        $youname = $searchResult['snippet']['title'];
        $start = strpos($youname, '- '); //will give the position of '- '
			if ( $start > 1 ) {
				$l = substr($youname, 0, $start); // Extract Artist
				$r = substr($youname, $start+2); // Extract Title
			} else { $r = $youname; $l = ""; }
			$artist = ucwords(strtolower(preg_replace('/[^\p{L}0-9&()\-]/u', ' ', trim(removeaccents($l)))));
			$title = ucwords(strtolower(preg_replace('/[^\p{L}0-9&()\-]/u', ' ', trim(removeaccents($r)))));
      switch ($searchResult['id']['kind']) {
        case 'youtube#video':
          $videos .= sprintf('<li><h2>%s</h2> %s</li>',
            $youname,
            "<div>
            <table><tr><td>
            <br><iframe id='ytplayer' type='text/html' width='320' height='240' src='https://www.youtube.com/embed/".$searchResult['id']['videoId']."' frameborder='0' allowfullscreen=''></iframe>
				</td></tr>
				<tr><td>&nbsp;&nbsp;&nbsp;</td></tr>
				<tr><td>
				<form action='./mp3copy.php'  method='GET'>
				  
				  <div>
				    <input type='hidden' id='id".$searchResult['id']['videoId']."' name='id' value='".$searchResult['id']['videoId']."'>
				    Artiste: <input type='text' id='artist".$searchResult['id']['videoId']."' name='artist' value='".$artist."'>
				  </div>
				  <div>
				    Titre: <input type='text' id='title".$searchResult['id']['videoId']."' name='title' value='".$title."'>
				  </div>
 	
  &#9835; <a href='javascript:swaptag(\"".$searchResult['id']['videoId']."\")'>Inversez</a>? 
  <a href='javascript:settag(\"".$searchResult['id']['videoId']."\")'>Corrigez</a> artiste et titre?
  <br>
				  <!--
				  <input type='radio' name='type' value='music' checked>Musique
				  <input type='radio' name='type' value='video'>Video (TODO)
				  -->
				  <br>
				  <div  style='text-align:center'>
				  <input type='submit' value='&#9835; COPIER &#9835;'>
				  </div>
				</form>
				</td></tr></table>
				</div>");
          break;
        case 'youtube#channel':
          $channels .= sprintf('<li>%s (%s)</li>', $youname,
            $searchResult['id']['channelId']);
          break;
       }
    }

   } catch (Google_ServiceException $e) {
    $htmlBody .= sprintf('<p>A service error occurred: <code>%s</code></p>',
      htmlspecialchars($e->getMessage()));
  } catch (Google_Exception $e) {
    $htmlBody .= sprintf('<p>An client error occurred: <code>%s</code></p>',
      htmlspecialchars($e->getMessage()));
  }
}
?>
<!doctype html>
<html>
  <head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta name="description" content="">
  <meta name="author" content="fred" >
  <meta http-equiv="Content-Type" content="text/html; charset=utf-8" >
  <title>[REC] - Youtube : <?php echo urldecode($_REQUEST['q']);?></title>
	<link href="./bootstrap.css" rel="stylesheet">
	
	<script type="text/javascript">
	function swaptag(id)
		{
			 var oldtitle = document.getElementById('title'+id);
			 var oldartist = document.getElementById('artist'+id);
			 var tmp = oldtitle.value; 
			 oldtitle.value = oldartist.value;
			 oldartist.value = tmp;  
		}
		
	function settag(id)
		{
			 var oldtitle = document.getElementById('title'+id);
			 var oldartist = document.getElementById('artist'+id);
			 var newA = decodeURIComponent( '<?php echo removeaccents($_REQUEST['artist']); ?>' );
			 var newT = decodeURIComponent( '<?php echo removeaccents($_REQUEST['title']); ?>' );
			 if (newT != '') oldtitle.value = newT;
			 if (newA != '') oldartist.value = newA;
		}
	</script>
  </head>
  <body>
<center>
   <section class="content-section text-center">
        <div class="result-section">
						<p>
						  <a href="/" >
						  <img src="youtube.jpg" alt="Recherche et copie mp3 Youtube"></a>
							<h3>Enregistrez la bande son de vos vidéos...</h3>
						    <form action="./search.php"  method="GET">
						  <div>
						    Recherche: <input type="search" id="q" name="q" placeholder="Faites votre recherche" value="<?php echo urldecode($_GET['q']); ?>">
						  </div>
						  <div>
						    Resultats: <input type="number" id="maxResults" name="maxResults" min="1" max="50" step="1" value="5">
						  </div>
						  <input type="submit" value="Lancer">
						</form>
						    <ul><?php echo $videos; ?></ul>
						</p>
			</div>
    </section>

   <section class="content-section text-center">
        <div class="appz-section">
					<p>
						<h2><a href="/torrent/?q=<?php echo urlencode( $_REQUEST['q']);?>">[REC] Recherchez sur Bittorrent...</a></h2>
					</p>
			</div>
    </section>
</center>
</body>
</html>
 