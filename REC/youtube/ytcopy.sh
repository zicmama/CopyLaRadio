#!/bin/bash -x
################################################################################
# Author: Fred (webmaster@zicmama.com)
# Version: 0.1
# License: GPL (http://www.google.com/search?q=GPL)
################################################################################
# INIT
# sudo apt-get install lame sox libsox-fmt-mp3 eyed3 libav-tools imagemagick
# Install last Youtube-dl release
# sudo wget https://yt-dl.org/downloads/latest/youtube-dl -O /usr/local/bin/youtube-dl
# sudo chmod a+x /usr/local/bin/youtube-dl
# sudo pip install beets
################################################################################
# ./ytcopy.sh cdocgTNxHAM "Kobo Town" "Kaiso Newcast"

# INDIQUER LE REPERTOIRE FINAL
prefixpath="/home/xbian/REC/MUSIC"


if [[ "$1" != "" && ( "$2" != "" || "$3" != "") ]]; then
	id=$1;
	artist="$2"
	title="$3"
	# TYPE 
	t=$4
	
	if [[ -z "$title" ]]; then
		title="Titre Inconnu";
	fi
	if [[ -z "$artist" ]]; then
		artist="Artiste Inconnu";
	fi

	yoump3=/tmp/${artist}\ -\ ${title}.$id.mp3
	you432=/tmp/${artist}\ -\ ${title}.$id.432Hz.mp3
	youtrim=/tmp/${artist}\ -\ ${title}.$id.trim.mp3
	
	if [ ! -d "${prefixpath}" ]
	then
  		mkdir "${prefixpath}"
	fi

	finalmp3=${prefixpath}/${artist}\ -\ ${title}.$id.mp3
	finalimg=${prefixpath}/${artist}\ -\ ${title}.$id.jpg

	youdest=/tmp/${artist}\ -\ ${title}.$id.%\(ext\)s
	mp3name=${artist}\ -\ ${title}.$id.mp3
	youimg=/tmp/$id.jpg
	watchURL="http://www.youtube.com/watch?v="$id;
	radio="[REC]"

	# LOG
	log="--album=\"${radio}\" --artist=\"${artist}\" --song=\"${title}\" --comment=\"http://copylaradio.com\"";
	echo $log >> /tmp/$id.log
	
	if [[ -f "${finalmp3}" ]]
	then
		# FILE EXISTS. Replace if size < 1 Mo.
		msize=`ls -sh "${finalmp3}" | cut -d "M" -f 1`;
		# Giga size support added
		if [[ $msize =~ "G" ]]; then msize=$((`du -sb . | cut -f 1`/1024/1024)); fi
		if [[ $msize =~ "K" ]]; then msize=1; fi
		zsize=$(printf %.0f $msize);
	fi
	
else
	echo "[ERROR] ytcopy.sh BAD PARAMETERS"
	exit;
fi


action=0;
if [[ ! -f "${finalmp3}" ]]
then
	action=1
fi
if [[ $zsize -le 1 ]]
then
	action=1;
fi


if [[ $action != 0 ]]
then
	#Copy Image
	wget -O ${youimg}.jpg http://i.ytimg.com/vi/$id/0.jpg >> /tmp/$id.log 2>&1
	# Make image square!! Better AppZ display
	convert ${youimg}.jpg -set option:size '%[fx:min(w,h)]x%[fx:min(w,h)]' xc:none +swap -gravity center -composite $youimg
	rm ${youimg}.jpg
	# TODO get image from last.fm
	# http://userserve-ak.last.fm/serve/500/26159947.jpg
	echo "youtube-dl --max-filesize 200m --extract-audio --output=\"${youdest}\" --audio-format=mp3 ${watchURL} > /tmp/$id.log 2>&1" >> /tmp/$id.log;
	youtube-dl --max-filesize 200m --extract-audio --output="${youdest}" --audio-format=mp3 ${watchURL} >> /tmp/$id.log 2>&1;
	
	# CONVERT SONG TO 432 Hz ;)
	#/usr/bin/sox "${yoump3}" "${you432}" pitch -31
	#yoump3="${you432}"

	# TRIM SILENCES (double copy time 1mn)
	#echo "# TRIM SILENCES" >> /tmp/$id.log 2>&1;
	#echo "/usr/bin/sox \"${yoump3}\" --norm \"${youtrim}\" silence 1 0.1 0.1% reverse silence 1 0.1 0.1% reverse" >> /tmp/$id.log 2>&1;
	#/usr/bin/sox "${yoump3}" --norm "${youtrim}" silence 1 0.1 0.1% reverse silence 1 0.1 0.1% reverse >> /tmp/$id.log 2>&1;
	#rm "${yoump3}"/
	#yoump3="${youtrim}"
	
	echo "TAG eyeD3: Embed FRONT_COVER"  >> /tmp/$id.log 2>&1;
	eyeD3 --remove-all "$yoump3"  >> /tmp/$id.log 2>&1;
	eyeD3 --album="[REC]" --artist="${artist}" --title="${title}" --add-image=${youimg}:FRONT_COVER --comment="::Zeebox Copy, http://copylaradio.com" "$yoump3"  >> /tmp/$id.log 2>&1;

	# USE BEET TO IMPORT & CLEAN COLLECTION
	beet import -s -q /tmp >> /tmp/$id.log 2>&1;
	# MOVE FINAL
	echo "mv -v \"${yoump3}\" \"$finalmp3\" >> /tmp/$id.log 2>&1" >> /tmp/$id.log ;
	mv -v "${yoump3}" "$finalmp3" >> /tmp/$id.log 2>&1;
#	mv -v "${youimg}" "${finalimg}" >> /tmp/$id.log 2>&1;
 	rm "${youimg}" >> /tmp/$id.log 2>&1;

else
	echo "[ERROR] ytcopy.sh :: finalmp3 OR size EXCEPTION"
	exit;
fi
